# tfetch

[![shell-test](https://github.com/AOrps/tfetch/actions/workflows/shell-test.yml/badge.svg)](https://github.com/AOrps/tfetch/actions/workflows/shell-test.yml)

T(iny)fetch : Meant to be a Portable and Minimal System Information Tool for Terminal Startup
* Hopefully also really performant too and relying on as few tools as possible
* This is meant to be extremely readable so it can be highly customize-able

# Please look at ##CREDITS for replacement

## Installation

```sh
# Clones repo into local machine
git clone https://github.com/AOrps/tfetch.git

# Changes Directory to be in tfetch
cd tfetch/

# Adds Execute Permission to file (if permission is not enabled)
chmod +x tfetch.sh
```

## Usage

```sh
# To Run the Script
./tfetch.sh
```

## Dependencies
```txt
whoami, uname, uptime, cut, rev, echo, date, sed, tput
```

## Credits
* Check the following if this repo is not what you are looking for

| Repo | Description
| :--- | :---
| [Neofetch](https://github.com/dylanaraps/neofetch) |    A command-line system information tool written in bash 3.2+
| [uFetch](https://gitlab.com/jschx/ufetch/-/tree/master) | Tiny system info for Unix-like operating systems.
| [pFetch](https://github.com/dylanaraps/pfetch) | A pretty system information tool written in POSIX sh. 

---

# Workflow
```yml
# This is a basic workflow to help you get started with Actions

name: shell

# Controls when the action will run. 
on:
  # Triggers the workflow on push or pull request events but only for the main branch
  push:
    branches: [ main ]
  pull_request:
    branches: [ main ]


# A workflow run is made up of one or more jobs that can run sequentially or in parallel
jobs:
  # This workflow contains a single job called "check"
  check:
    # The type of runner that the job will run on
    runs-on: ubuntu-latest

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Add Execute Permission to File
        run: chmod +x "${GITHUB_WORKSPACE}/tfetch.sh"

      # Runs a set of commands using the runners shell
      - name: Run tfetch.sh script
        run:  "${GITHUB_WORKSPACE}/tfetch.sh"
```

---

# Ascii Art
* Feel free to add your own designs for your individual OS logo

* Keep it 7 lines

* Ensure that equal line has an equal amount of characters

## Arch
```txt
        /\\         
       /  \\        
      /\\   \\      
     /  __  \\      
    /  (  )  \\     
   / __|  |__\\\\   
  /.\`        \`.\\  
```
## Macos
```txt
         .:'    
      __ :'__   
   .'`  `-'  ``.
  :          .-'
  :         :   
   :         `-;
    `.__.-.__.'  
```

### Variant 1
```txt
         .:'    
      __ :'__   
   .'`__`-'__``.
  :__________.-'
  :_________:   
   :_________`-;
jgs `.__.-.__.'  
```

## Manjaro
```txt
||||||||| ||||
||||||||| ||||
||||      ||||
|||| |||| ||||
|||| |||| ||||
|||| |||| ||||
|||| |||| |||| 
```
## Ubuntu
```txt
           _   
       ---(_)  
   _/  ---  \\ 
  (_) |   |   |
    \\  --- _/ 
       ---(_)  
                
```

---

# License

```txt
MIT License

Copyright (c) 2021 ;)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```
---

# Script 

```sh
: '
Should be able to run on the following shells:  
sh, bash (zsh not with it bc of readarray)
'
# Fields 
# Need 6-7 fields
user="$(whoami)"
host="$(uname -n)"
os="" # Enter Operating System ex. os="Manjaro" ex. os="MacOS"
kernel="$(uname -sr | cut -d- -f1)"
uptime="$(uptime | cut -d "p" -f2 | cut -b 2- | cut -d "u" -f1 | rev | cut -b 6- | rev)" # $(uptime -p for Linux Systems is cooler 😎 ) 
shell="$(echo ${SHELL} | cut -b 6-)"
day="$(date +'%D')"
time="$(date +'%r')"


url="https://raw.githubusercontent.com/AOrps/tfetch/main/ascii-art/${os,,}.txt"
F=$(curl -s ${url})

readarray -t strarr <<< "${F}"

for ((n=0; n < ${#strarr[*]}; n++))
do
    echo "${strarr[n]} - bruh"
done

# Colors 
# This is completely for the design of 

# Depending on what is larger
: '
Depending on what is larger 
this will be able to dynamically get the 
'


# All the magic, this will be printed out
cat <<EOF

$(sed '1q;d' ascii-art/${os,,}.txt)  ${user}@${host}
$(sed '2q;d' ascii-art/${os,,}.txt)  -------------------
$(sed '3q;d' ascii-art/${os,,}.txt)  OS      ${os}
$(sed '4q;d' ascii-art/${os,,}.txt)  Kernel  ${kernel}
$(sed '5q;d' ascii-art/${os,,}.txt)  Date    ${day}
$(sed '6q;d' ascii-art/${os,,}.txt)  Uptime  ${uptime}
$(sed '7q;d' ascii-art/${os,,}.txt)  Shell   ${shell}

EOF
```